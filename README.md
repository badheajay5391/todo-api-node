# todo-api

## Download Instruction
 
`git clone https://gitlab.com/badheajay5391/todo-api-node.git` 


## Database Setup

Download and isntall MogoDB.
Make sure that the database is running at `127.0.0.1:27017`


## Project Setup

Now run `cd todo-api` to go inside the directory

Run `npm install` to install all the dependencies.

Run `npm install -g nodemon` to install Nodemon Globally.

Run `npm start` to run the NodeJS Server.
